

let buttons = document.querySelectorAll('button')
let open = document.querySelector('#open')
let modal = document.querySelector('#lead')
let bg = document.querySelector('#bg')
let close = document.querySelector('#close')


const openModal = (params) => {
      const types = {
            Sign_btn: {
                  title: 'Записаться на открытый урок'
            },
            Sign_up: {
                  title: 'Записаться на курс'
            },
            Call_me: {
                  title: 'Позвоните Нам'
            }
      }

      lead.classList.add('active')

      close.onclick = () => {
            modal.classList.remove('active')
            bg.classList.remove('active')
      }

      lead.querySelector('h2').innerHTML = types[params.getAttribute('data-type')].title
}

for (let item of buttons) item.onclick = () => {
      openModal(item)
}

const form = document.getElementById('form');
const username = document.getElementById('username');


form.addEventListener('submit', e => {
	e.preventDefault();
	
	checkInputs();
});

function checkInputs() {
	const usernameValue = username.value.trim();
	
	if(usernameValue === '') {
		setErrorFor(username, 'Name Cards?');
	} else {
		setSuccessFor(username);
	}
    
}

function setErrorFor(input, message) {
	const formControl = input.parentElement;
	const small = formControl.querySelector('small');
	formControl.className = 'form-control error';
	small.innerText = message;
}

function setSuccessFor(input) {
	const formControl = input.parentElement;
	formControl.className = 'form-control success';
}
	
function isEmail(email) {
	return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}

const floating_btn = document.querySelector('.floating-btn');
const close_btn = document.querySelector('.close-btn');
const social_panel_container = document.querySelector('.social-panel-container');

floating_btn.addEventListener('click', () => {
	social_panel_container.classList.toggle('visible')
});

close_btn.addEventListener('click', () => {
	social_panel_container.classList.remove('visible')
});